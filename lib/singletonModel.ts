import { Observable } from 'rxjs';

import { HttpUtility } from 'rl-http';
import { BaseModel } from './baseModel';

export class SingletonModel<T> extends BaseModel<T> {
	transform: { (data: any): T };

	constructor(url: string, http: HttpUtility) {
		super(url, http);
		this.transform = data => data;
	}

	update(newModel: T): Observable<T> {
		const request = this.http.put<T>(this.url, newModel).map(this.transform).share();
		request.subscribe(model => this.dataStream$.next(model));
		return request;
	}

	load(): Observable<T> {
		const request = this.http.get<T>(this.url).map(this.transform).share();
		request.subscribe(model => this.dataStream$.next(model));
		return request;
	}
}
