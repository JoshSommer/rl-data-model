import { BaseModel } from './baseModel';

describe('BaseModel', () => {
	let model: BaseModel<number>;
	let http: any;

	beforeEach(() => {
		http = {};
		model = new BaseModel<number>('/test', http);

		expect(model.url).to.equal('/test');
	});

	it('should initialize on the first subscription', () => {
		let item = 11;
		const loadSpy = sinon.spy();
		model.load = loadSpy;

		model.subscribe(data => item = data);

		sinon.assert.calledOnce(loadSpy);
		expect(item).to.be.null;
	});

	it('should return an empty observable', () => {
		const nextSpy = sinon.spy();
		const completeSpy = sinon.spy();

		model.load().subscribe(nextSpy, null, completeSpy);

		sinon.assert.notCalled(nextSpy);
		sinon.assert.calledOnce(completeSpy);
	});
});
